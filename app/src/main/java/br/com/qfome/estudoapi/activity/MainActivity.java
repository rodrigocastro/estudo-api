package br.com.qfome.estudoapi.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import br.com.qfome.estudoapi.R;
import br.com.qfome.estudoapi.api.ApiClient;
import br.com.qfome.estudoapi.api.response.UserResponse;
import br.com.qfome.estudoapi.model.User;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity implements Callback<UserResponse> {

    @InjectView(R.id.email)
    EditText email;
    @InjectView(R.id.password)
    EditText password;
    @InjectView(R.id.loading)
    ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.send)
    public void send(View view) {
        Toast.makeText(this, "Registrar!", Toast.LENGTH_SHORT).show();
        String email = this.email.getText().toString();
        String password = this.password.getText().toString();

        if (email.isEmpty() || password.isEmpty()){
            Toast.makeText(this, getString(R.string.email_password_empty), Toast.LENGTH_LONG).show();
        }else{
            loading.setVisibility(View.VISIBLE);
            User user = new User();
            user.email = email;
            user.password = password;
            ApiClient.getClient().register(user, this);
        }
    }

    @Override
    public void success(UserResponse userResponse, Response response) {
        if (userResponse.token != null){
            Toast.makeText(getApplicationContext(), "Usuário cadastrado com sucesso. Token:"+userResponse.token, Toast.LENGTH_SHORT).show();
        }else{
            failure(null);
        }
        loading.setVisibility(View.GONE);
    }

    @Override
    public void failure(RetrofitError error) {

    }
}
