package br.com.qfome.estudoapi.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rodrigo on 12/9/14.
 */
public class User {

    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;

}
