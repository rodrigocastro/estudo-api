package br.com.qfome.estudoapi.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.qfome.estudoapi.api.response.UserResponse;
import br.com.qfome.estudoapi.model.User;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by rodrigo on 12/9/14.
 */
public class ApiClient {

    public static final String BASE_URL = "http://reqr.es/api/";

    public static Routes sharedClient;





    public static Routes getClient(){
        if (sharedClient == null){
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create();
            //String token = JuridicoCorrespondentesApplication.getTokenUser();
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setConverter(new GsonConverter(gson))
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint(String.format("%s", BASE_URL))
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            request.addHeader("WWW-Authenticate", "none");
                            //String token = JuridicoCorrespondentesApplication.getTokenUser();
//                            if (token != null && !token.isEmpty()) {
//                                String authorization = String.format("TRUEREST token=%s&apikey=3ebcfcc24cb63acaba81c731785c0e287fef3163", token);
//                                request.addHeader("Authorization", authorization);
//                            } else {
//                                request.addHeader("Authorization", "TRUEREST apikey=3ebcfcc24cb63acaba81c731785c0e287fef3163");
//                            }
                        }
                    })
                    .build();
            sharedClient = restAdapter.create(Routes.class);
        }

        return sharedClient;
    }

    public interface Routes{

        @POST("/register")
        public void register(@Body User user, Callback<UserResponse> callback);

    }
}
