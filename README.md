#Estudo API#

O objetivo deste projeto é praticar e assim criar um aplicativo android com comunicação com uma API fake.

### Setup ###

git clone https://rodrigocastro@bitbucket.org/rodrigocastro/estudo-api.git

cd estudo-api

git push origin develop

git flow feature start api/seunome

###Links úteis ###

API: http://reqr.es/

Lib de manipulação de API: http://square.github.io/retrofit/

Lib de manipulação de View:  http://jakewharton.github.io/butterknife/